// @flow

import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Link from '@material-ui/core/Link';
import teal from '@material-ui/core/colors/teal';
import CommentIcon from '@material-ui/icons/CommentOutlined';
import { connect } from 'react-redux';

import { deleteTicket, setCurrentTicket } from '../../actions/ticketActions';
import { Divider } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  paper: {
    maxWidth: 520,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
  paperBg: {
    maxWidth: 520,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
    backgroundColor: teal[500],
  },
}));

const getPriorityColor = p => {
  switch (p) {
    case 'LOW':
      return 'textSecondary';
    case 'Normal':
      return 'primary';
    case 'HIGHT':
      return 'error';
    default:
      return 'primary';
  }
};

type Props = {
  ticket: {
    id: number,
    comment: ?string,
    description: string,
    priority: string,
    creator: any,
    affectTo: any,
    createdAt: string,
    resolved: boolean,
  },
  handleOpenDialog: boolean => void,
  deleteTicket: number => void,
  setCurrentTicket: data => void,
};

function TicketItem(props: Props) {
  const classes = useStyles();
  const { ticket, handleOpenDialog, deleteTicket, setCurrentTicket } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelectTicket = () => {
    setCurrentTicket(ticket);
    handleOpenDialog(true);
  };

  const handleDeleteTicket = () => {
    deleteTicket(ticket.id);
  };

  return (
    <Paper className={ticket.resolved ? classes.paperBg : classes.paper}>
      <Grid container wrap="nowrap" spacing={2}>
        <Grid item>
          {ticket.affectTo ? (
            <Tooltip
              title={ticket.affectTo.name}
              aria-label={ticket.affectTo.name}
            >
              <Avatar>{ticket.affectTo.name.charAt(0)}</Avatar>
            </Tooltip>
          ) : (
            ''
          )}
        </Grid>
        <Grid item xs zeroMinWidth>
          <Typography noWrap>
            <Link
              onClick={handleSelectTicket}
              color={getPriorityColor(ticket.priority)}
            >
              {`#ID${ticket.id}`}
            </Link>
          </Typography>
          <Typography
            variant="body1"
            wrap="true"
          >{`${ticket.description}`}</Typography>
          <Typography variant="caption" color="textSecondary" display="inline">
            {ticket.createdAt}
          </Typography>
          <Typography variant="caption" color="textSecondary" display="inline">
            {ticket.creator ? ` By ${ticket.creator.name}` : ''}
          </Typography>
          <Typography variant="caption" color="textSecondary" display="block">
            {ticket.comment ? (
              <Fragment>
                <CommentIcon color="primary" fontSize="small" />
                {ticket.comment}
              </Fragment>
            ) : (
              ''
            )}
          </Typography>
        </Grid>
        <Grid item>
          <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="long-menu"
            keepMounted
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            PaperProps={{
              style: {},
            }}
          >
            <MenuItem key="delete-ticket" onClick={handleDeleteTicket}>
              Delete
            </MenuItem>
          </Menu>
        </Grid>
      </Grid>
    </Paper>
  );
}
const mapDispatchToProps = dispatch => ({
  deleteTicket: (id: number) => dispatch(deleteTicket(id)),
  setCurrentTicket: (data: any) => dispatch(setCurrentTicket(data)),
});

export default connect(null, mapDispatchToProps)(TicketItem);
