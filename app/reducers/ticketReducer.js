// @flow

import {
  GET_TICKETS,
  PATCH_TICKET,
  DELETE_TICKET,
  CREATE_TICKET,
  TICKET_LOADING,
  TICKET_ERROR,
  SET_CURRENT_TICKET,
} from '../actions/type';

type State = {
  items: [],
  isLoading: boolean,
  currentItem: Object,
  error: any,
};

type Action = { type: string, payload: any };

const initialState: State = {
  items: [],
  isLoading: false,
  currentItem: null,
  error: null,
};
export default function(state: State = initialState, action: Action) {
  switch (action.type) {
    case TICKET_LOADING:
      return { ...state, isLoading: action.payload };
    case CREATE_TICKET:
      return { ...state, items: [...state.items, action.payload] };
    case GET_TICKETS:
      return { ...state, items: action.payload };
    case PATCH_TICKET:
      return {
        ...state,
        items: state.items.map((item, index) =>
          item.id === action.payload.id ? action.payload : item,
        ),
      };
    case DELETE_TICKET:
      return {
        ...state,
        items: state.items.filter(
          (item, index) => item.id !== action.payload.id,
        ),
      };
    case TICKET_ERROR:
      return { ...state, error: action.payload };

    case SET_CURRENT_TICKET:
      return { ...state, currentItem: action.payload };

    default:
      return state;
  }
}
