// @flow

import React, { useState, useEffect } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PostAddIcon from '@material-ui/icons/PostAdd';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import { connect } from 'react-redux';
import { patchTicket } from '../../actions/ticketActions';

type Props = {
  open: boolean,
  handleOnClick: boolean => void,
  currentTicket: any,
  users: [],
  updateTicket: (*) => void,
};

const useStyles = makeStyles(theme => ({
  root: {},
  inputWrapper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  selectWrapper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  divider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function EditTicketFormDialog(props: Props) {
  const { open, handleOnClick, currentTicket, users, updateTicket } = props;
  const [description, setDescription] = useState('');
  const [comment, setComment] = useState('');
  const [resolved, setResolved] = useState(false);
  const [priority, setPriority] = useState('');
  const [affectTo, setAffectTo] = useState('');
  const classes = useStyles();

  useEffect(() => {
    if (currentTicket) {
      setDescription(currentTicket.description);
      setPriority(currentTicket.priority);
      setAffectTo(currentTicket.affectTo.id);
      setComment(currentTicket.comment || '');
      setResolved(currentTicket.resolved);
    }
  }, [currentTicket]);

  const handleSubmit = event => {
    event.preventDefault();
    const data = {
      description,
      priority,
      affectTo: { id: affectTo },
      comment,
      resolved,
    };
    updateTicket(currentTicket.id, data);
    handleOnClick(false);
  };

  const toggleChecked = () => {
    setResolved(prev => !prev);
    console.log(resolved);
  };

  const handleChange = event => {
    if (event.target.name == 'priority') setPriority(event.target.value);
    if (event.target.name == 'affectto') setAffectTo(event.target.value);
    if (event.target.name == 'description') setDescription(event.target.value);
    if (event.target.name == 'comment') setComment(event.target.value);
  };

  if (!currentTicket) return '';

  return (
    <Dialog
      open={open}
      onClose={() => handleOnClick(false)}
      aria-labelledby="form-dialog-title"
      fullWidth
    >
      <form autoComplete="off" onSubmit={handleSubmit}>
        <DialogTitle id="form-dialog-title">
          <Typography variant="body1">
            <PostAddIcon color="primary" />
            Edit Ticket
          </Typography>
        </DialogTitle>
        <DialogContent>
          <div className={classes.inputWrapper}>
            <TextField
              name="description"
              autoFocus
              fullWidth
              multiline
              rows="3"
              margin="dense"
              label="Ticket"
              variant="outlined"
              onChange={handleChange}
              value={description}
            />
          </div>
          <div className={classes.selectWrapper}>
            <InputLabel shrink id="ticket-priority">
              Priority
            </InputLabel>
            <Select
              name="priority"
              labelId="ticket-priority"
              value={priority}
              onChange={handleChange}
              displayEmpty
              className={classes.selectEmpty}
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value="LOW">Low</MenuItem>
              <MenuItem value="NORMAL">Normal</MenuItem>
              <MenuItem value="HIGHT">Hight</MenuItem>
            </Select>
          </div>

          <div className={classes.selectWrapper}>
            <InputLabel shrink id="ticket-affect-to">
              Affect to
            </InputLabel>
            <Select
              name="affectto"
              labelId="ticket-affect-to"
              value={affectTo}
              onChange={handleChange}
              className={classes.selectEmpty}
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {users.map(u => (
                <MenuItem value={u.id} key={u.id}>
                  {u.name}
                </MenuItem>
              ))}
            </Select>
          </div>
          <div className={classes.inputWrapper}>
            <TextField
              name="comment"
              autoFocus
              fullWidth
              multiline
              rows="2"
              margin="dense"
              label="Comment (optional)"
              variant="outlined"
              onChange={handleChange}
              value={comment}
            />
          </div>
          <div className={classes.inputWrapper}>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    checked={resolved}
                    onChange={toggleChecked}
                    color="primary"
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                  />
                }
                label="Ticket Resolved"
              />
            </FormGroup>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleOnClick(false)} color="primary">
            Cancel
          </Button>
          <Button color="primary" type="submit" variant="contained">
            Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

const mapStateToProps = state => ({
  currentTicket: state.ticket.currentItem,
  users: state.user.items,
});

const mapDispatchToProps = dispatch => ({
  updateTicket: (id: number, data: any) => dispatch(patchTicket(id, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditTicketFormDialog);
