// @flow

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik } from 'formik';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { createUser } from '../../actions/userActions';

type Props = {
  open: boolean,
  handleOnClick: boolean => void,
  createUser: (*) => void,
};

// eslint-disable-next-line arrow-parens
const useStyles = makeStyles(theme => ({
  root: {
    width: 500,
  },
  formRoot: {
    margin: theme.spacing(1),
  },
  inputWrapper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  actionsWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: theme.spacing(3),
  },
}));

function AddUserFormDialog(props: Props) {
  const { open, handleOnClick, createUser } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={() => handleOnClick(false)}
        aria-labelledby="form-dialog-title"
        fullWidth
      >
        <DialogTitle id="form-dialog-title">
          <Typography variant="body1">
            <PersonAddIcon color="primary" /> Add User
          </Typography>
        </DialogTitle>
        <Divider />
        <DialogContent>
          <Formik
            initialValues={{ name: '', email: '', password: '' }}
            // eslint-disable-next-line arrow-parens
            validate={values => {
              const errors = {};
              // name field  validation
              if (!values.name) {
                errors.name = 'The name field is required';
              }
              // email field validation
              if (!values.email) {
                errors.email = 'The email field is required';
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = 'Invalid email address';
              }
              // password field validation
              if (!values.password) {
                errors.password = 'The password field is required';
              } else if (values.password.length < 6) {
                errors.password =
                  'The password is too short (min 6 characters)';
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              createUser(values);
              handleOnClick(false);
              setSubmitting(false);
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <form
                onSubmit={handleSubmit}
                className={classes.formRoot}
                autoComplete="off"
              >
                <div className={classes.inputWrapper}>
                  <TextField
                    fullWidth
                    type="text"
                    name="name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    defaultValue={values.name}
                    label={errors.name && touched.name ? errors.name : 'Name'}
                    variant="outlined"
                    error={errors.name && touched.name ? true : false}
                  />
                </div>
                <div className={classes.inputWrapper}>
                  <TextField
                    fullWidth
                    type="email"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    defaultValue={values.email}
                    label={
                      errors.email && touched.email ? errors.email : 'Email'
                    }
                    variant="outlined"
                    error={errors.email && touched.email ? true : false}
                  />
                </div>
                <div className={classes.inputWrapper}>
                  <TextField
                    error={errors.password && touched.password ? true : false}
                    fullWidth
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    label={
                      errors.password && touched.password
                        ? errors.password
                        : 'Password'
                    }
                    variant="outlined"
                  />
                </div>
                <div className={classes.actionsWrapper}>
                  <Button onClick={() => handleOnClick(false)} color="primary">
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    disabled={isSubmitting}
                  >
                    Submit
                  </Button>
                </div>
              </form>
            )}
          </Formik>
        </DialogContent>
      </Dialog>
    </div>
  );
}
const mapDispatcgToProps = (dispatch: any) => ({
  createUser: (data: any) => dispatch(createUser(data)),
});

export default connect(null, mapDispatcgToProps)(AddUserFormDialog);
