// @flow
import {
  USER_LOADING,
  CREATE_USER,
  GET_USERS,
  PATCH_USER,
  DELETE_USER,
  USER_ERROR,
} from '../actions/type';

const initialState = {
  items: [],
  isLoading: false,
  currentUser: null,
};

type State = {
  items: [],
  isLoading: boolean,
  currentUser: Object,
};

type Action = {
  type: string,
  payload: any,
};
export default function(state: State = initialState, action: Action) {
  switch (action.type) {
    case USER_LOADING:
      return { ...state, isLoading: action.payload };
    case CREATE_USER:
      return { ...state, items: [...state.items, action.payload] };
    case GET_USERS:
      return { ...state, items: action.payload };
    case PATCH_USER:
      return {
        ...state,
        items: state.items.map((item, index) =>
          item.id === action.payload.id ? action.payload : item,
        ),
      };
    case DELETE_USER:
      return {
        ...state,
        items: state.items.filter(
          (item, index) => item.id !== action.payload.id,
        ),
      };
    case USER_ERROR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
}
