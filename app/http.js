// @flow

import axios from 'axios';

const baseHeaders = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
  Accept: 'application/json',
};
const get = (url: string) => {
  return axios.get(url, { headers: baseHeaders });
};

const post = (url: string, data: any, config: Object = {}) => {
  const requestHeaders = Object.assign({}, baseHeaders, config);
  return axios.post(url, data, { header: requestHeaders });
};

const put = (url: string, data: any, config: Object = {}) => {
  const requestHeaders = Object.assign({}, baseHeaders, config);
  return axios.put(url, data, { headers: requestHeaders });
};

const patch = (url: string, data: any, config: Object = {}) => {
  const requestHeaders = Object.assign({}, baseHeaders, config);
  return axios.patch(url, data, { headers: requestHeaders });
};

const httpDelete = (url: string, config: Object = {}) => {
  const requestHeaders = Object.assign({}, baseHeaders, config);
  return axios.delete(url, { headers: requestHeaders });
};

export { get, post, put, patch, httpDelete };
