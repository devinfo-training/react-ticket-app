// @flow
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux';
import TicketItem from './TicketItem.component';

type Props = {
  handleOpenDialog: boolean => void,
  tickets: [],
  isLoading: boolean,
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    padding: theme.spacing(0, 3),
  },
}));

function TicketList(props: Props) {
  const classes = useStyles();
  const { handleOpenDialog, tickets, isLoading } = props;

  if (isLoading)
    return (
      <div className={classes.root}>
        <LinearProgress />
      </div>
    );

  return (
    <div className={classes.root}>
      {tickets.map((ticket, index) => (
        <TicketItem
          ticket={ticket}
          handleOpenDialog={handleOpenDialog}
          key={index}
        />
      ))}
    </div>
  );
}
const mapStateToProps = state => ({
  tickets: state.ticket.items,
  isLoading: state.ticket.isLoading,
});

export default connect(mapStateToProps)(TicketList);
