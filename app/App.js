// @flow
import React, { useState, useEffect } from 'react';
import { ThemeProvider } from 'styled-components';
import NoSsr from '@material-ui/core/NoSsr';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import { connect } from 'react-redux';

import NavBar from './components/layaout/Navbar.component';
import TicketList from './components/ticket/TicketList.component';
import FloatButton from './components/layaout/FloatButton.component';
import AddTicketFormDialog from './components/ticket/AddTicketFormDialog.component';
import AddUserFormDialog from './components/user/AddUserFormDialog.component';
import EditTicketFormDialog from './components/ticket/EditTicketFormDialog.component';
import { fetchUsers } from './actions/userActions';
import { fetchTickets } from './actions/ticketActions';

const theme = createMuiTheme();

// eslint-disable-next-line arrow-parens
const useStyle = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
  },
}));

type Props = {
  fetchTickets: () => void,
  fetchUsers: () => void,
};

function App(props: Props) {
  const classes = useStyle();
  const { fetchTickets, fetchUsers } = props;
  const [addTicketDialogOpen, setAddTicketDialogOpen] = useState(false);
  const [addUserDialogOpen, setAddUserDialogOpen] = useState(false);
  const [editTicketDialogOpen, setEditTicketDialogOpen] = useState(false);

  useEffect(() => {
    fetchUsers();
  }, []);

  useEffect(() => {
    fetchTickets();
  }, []);

  return (
    <NoSsr>
      <ThemeProvider theme={theme}>
        <NavBar />
        <Container className={classes.root}>
          <TicketList handleOpenDialog={setEditTicketDialogOpen} />
          <FloatButton
            handleOpenAddTicketDialog={setAddTicketDialogOpen}
            handleOpenAddUserDialog={setAddUserDialogOpen}
          />
          <AddTicketFormDialog
            open={addTicketDialogOpen}
            handleOnClick={setAddTicketDialogOpen}
          />
          <AddUserFormDialog
            open={addUserDialogOpen}
            handleOnClick={setAddUserDialogOpen}
          />

          <EditTicketFormDialog
            open={editTicketDialogOpen}
            handleOnClick={setEditTicketDialogOpen}
          />
        </Container>
      </ThemeProvider>
    </NoSsr>
  );
}
const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
  fetchTickets: () => dispatch(fetchTickets()),
});

export default connect(null, mapDispatchToProps)(App);
