// @flow

import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import EditIcon from '@material-ui/icons/Edit';
import PersonIcon from '@material-ui/icons/Person';

type Props = {
  handleOpenAddTicketDialog: boolean => void,
  handleOpenAddUserDialog: boolean => void,
};
export default function FloatButton(props: Props) {
  const { handleOpenAddTicketDialog, handleOpenAddUserDialog } = props;

  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleAddTicket = () => {
    handleOpenAddTicketDialog(true);
    setOpen(false);
  };

  const handleAddUser = () => {
    handleOpenAddUserDialog(true);
    setOpen(false);
  };

  return (
    <div className={classes.exampleWrapper}>
      <SpeedDial
        ariaLabel="add ticket actions"
        className={classes.speedDial}
        icon={<SpeedDialIcon />}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
      >
        <SpeedDialAction
          key="create-ticket"
          icon={<EditIcon />}
          tooltipTitle="Create new ticket"
          onClick={handleAddTicket}
        />
        <SpeedDialAction
          key="add-user"
          icon={<PersonIcon />}
          tooltipTitle="Add user"
          onClick={handleAddUser}
        />
      </SpeedDial>
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  exampleWrapper: {
    position: 'absoute',
    marginTop: theme.spacing(3),
  },
  radioGroup: {
    margin: theme.spacing(1, 0),
  },
  speedDial: {
    position: 'absolute',
    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
      top: theme.spacing(2),
      left: theme.spacing(2),
    },
  },
}));
