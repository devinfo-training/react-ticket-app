// @flow

import { USER_LOADING, GET_USERS, USER_ERROR, CREATE_USER } from './type';

import { get, post } from '../http';
import { API_URI } from '../constants';

const createAction = (type, payload) => ({
  type,
  payload,
});

export const fetchUsers = () => (dispatch: any) => {
  dispatch(createAction(USER_LOADING, true));

  get(`${API_URI}/users`)
    .then(response => {
      if (response.status === 200)
        dispatch(createAction(GET_USERS, response.data));
    })
    .catch(error => dispatch(createAction(USER_ERROR, error)))
    .finally(() => dispatch(createAction(USER_LOADING, false)));
};

export const createUser = (data: any) => (dispatch: any) => {
  dispatch(createAction(USER_LOADING, true));
  post(`${API_URI}/user`, data)
    .then(response => {
      if (response.status === 200) {
        dispatch(createAction(CREATE_USER, response.data));
      }
    })
    .catch(error => dispatch(createAction(USER_ERROR, error)))
    .finally(() => dispatch(createAction(USER_LOADING, false)));
};
