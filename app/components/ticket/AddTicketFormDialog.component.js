// @flow

import React, { useState } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PostAddIcon from '@material-ui/icons/PostAdd';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';
import { createTicket } from '../../actions/ticketActions';

type Props = {
  open: boolean,
  handleOnClick: boolean => void,
  users: [],
  createTicket: (*) => void,
};

const useStyles = makeStyles(theme => ({
  root: {},

  inputWrapper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  selectWrapper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function AddTicketFormDialog(props: Props) {
  const { open, handleOnClick, users } = props;
  const creator = { id: 1 };
  const [description, setDescription] = useState('');
  const [priority, setPriority] = useState('');
  const [affectTo, setAffectTo] = useState('');
  const classes = useStyles();

  // eslint-disable-next-line arrow-parens
  const handleSubmit = event => {
    event.preventDefault();
    const ticket = {
      description,
      priority,
      creator,
      affectTo: { id: affectTo },
    };
    props.createTicket(ticket);
    handleOnClick(false);
    // clean up the form
    setDescription('');
    setPriority('');
    setAffectTo('');
  };
  // eslint-disable-next-line arrow-parens
  const handleChange = event => {
    if (event.target.name == 'priority') setPriority(event.target.value);
    if (event.target.name == 'affectto') setAffectTo(event.target.value);
    if (event.target.name == 'description') setDescription(event.target.value);
  };

  return (
    <Dialog
      open={open}
      onClose={() => handleOnClick(false)}
      aria-labelledby="form-dialog-title"
      fullWidth
    >
      <form autoComplete="off" onSubmit={handleSubmit}>
        <DialogTitle id="form-dialog-title">
          <Typography variant="body1">
            <PostAddIcon color="primary" />
            New Ticket
          </Typography>
        </DialogTitle>
        <DialogContent>
          <div className={classes.inputWrapper}>
            <TextField
              name="description"
              autoFocus
              fullWidth
              multiline
              rows="3"
              margin="dense"
              label="Ticket"
              variant="outlined"
              onChange={handleChange}
              value={description}
            />
          </div>
          <div className={classes.selectWrapper}>
            <InputLabel shrink id="ticket-priority">
              Priority
            </InputLabel>
            <Select
              name="priority"
              labelId="ticket-priority"
              value={priority}
              onChange={handleChange}
              displayEmpty
              className={classes.selectEmpty}
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value="LOW">Low</MenuItem>
              <MenuItem value="NORMAL">Normal</MenuItem>
              <MenuItem value="HIGHT">Hight</MenuItem>
            </Select>
          </div>

          <div className={classes.selectWrapper}>
            <InputLabel shrink id="ticket-affect-to">
              Affect to
            </InputLabel>
            <Select
              name="affectto"
              labelId="ticket-affect-to"
              value={affectTo}
              onChange={handleChange}
              className={classes.selectEmpty}
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {users.map(u => (
                <MenuItem value={u.id} key={u.id}>
                  {u.name}
                </MenuItem>
              ))}
            </Select>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleOnClick(false)} color="primary">
            Cancel
          </Button>
          <Button color="primary" type="submit">
            Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

const mapStateToPros = state => ({
  users: state.user.items,
});

const mapDispatchToProps = dispatch => ({
  createTicket: (data: any) => dispatch(createTicket(data)),
});

export default connect(mapStateToPros, mapDispatchToProps)(AddTicketFormDialog);
