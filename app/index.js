import React from 'react';
import ReactDom from 'react-dom';
import Root from './Root';
// global style
import './app.css';

ReactDom.render(<Root />, document.getElementById('app'));
