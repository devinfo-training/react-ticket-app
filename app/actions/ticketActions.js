// @flow

import {
  TICKET_LOADING,
  GET_TICKETS,
  TICKET_ERROR,
  DELETE_TICKET,
  CREATE_TICKET,
  SET_CURRENT_TICKET,
  PATCH_TICKET,
} from './type';
import { get, post, httpDelete, patch } from '../http';
import { API_URI } from '../constants';

const createAction = (type, payload) => ({
  type,
  payload,
});

export const fetchTickets = () => (dispatch: any) => {
  dispatch(createAction(TICKET_LOADING, true));
  get(`${API_URI}/tickets`)
    // eslint-disable-next-line arrow-parens
    .then(response => {
      if (response.status === 200)
        dispatch(createAction(GET_TICKETS, response.data));
    })
    // eslint-disable-next-line arrow-parens
    .catch(error => dispatch(createAction(TICKET_ERROR, error)))
    .finally(() => dispatch(createAction(TICKET_LOADING, false)));
};

export const deleteTicket = (id: number) => (dispatch: any) => {
  dispatch(createAction(TICKET_LOADING, true));
  httpDelete(`${API_URI}/ticket/${id}`)
    // eslint-disable-next-line arrow-parens
    .then(response => {
      if (response.data === 'Deleted' && response.status === 202) {
        dispatch(createAction(DELETE_TICKET, { id: id }));
      }
    })
    // eslint-disable-next-line arrow-parens
    .catch(error => dispatch(createAction(TICKET_ERROR, error)))
    .finally(() => dispatch(createAction(TICKET_LOADING, false)));
};

export const createTicket = (ticket: any) => (dispatch: any) => {
  dispatch(createAction(TICKET_LOADING, true));
  post(`${API_URI}/ticket`, ticket)
    .then(response => {
      if (response.status === 200) {
        dispatch(createAction(CREATE_TICKET, response.data));
      }
    })
    .catch(error => dispatch(createAction(TICKET_ERROR, error)))
    .finally(() => dispatch(createAction(TICKET_LOADING, false)));
};

export const setCurrentTicket = (data: any) => (dispatch: any) => {
  dispatch(createAction(SET_CURRENT_TICKET, data));
};

export const patchTicket = (id: number, data: any) => (dispatch: any) => {
  dispatch(createAction(TICKET_LOADING, true));
  patch(`${API_URI}/ticket/${id}`, data)
    .then(response => {
      if (response.status === 200) {
        dispatch(createAction(PATCH_TICKET, response.data));
      }
    })
    .catch(error => dispatch(createAction(TICKET_ERROR, error)))
    .finally(() => dispatch(createAction(TICKET_LOADING, false)));
};
