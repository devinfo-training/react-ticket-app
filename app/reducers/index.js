import { combineReducers } from 'redux';
import ticketReducer from './ticketReducer';
import userReducer from './userReducer';

export const rootReducer = combineReducers({
  user: userReducer,
  ticket: ticketReducer,
});
